﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lab1WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            txtAnalize.TextChanged += TxtAnalize_TextChanged;
        }
        
        private void TxtAnalize_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtAnalize.Text.Length == 0)
            {
                txtAnalize.Background = Brushes.Transparent;
            }
            else
            {
                if (Solve(txtAnalize.Text))
                {
                    txtAnalize.Background = new SolidColorBrush(Color.FromArgb(0x33, 0, 0xFF, 0));
                }
                else
                {
                    txtAnalize.Background = new SolidColorBrush(Color.FromArgb(0x33, 0xFF, 0, 0));
                }
            }
        }

        /// <summary>
        /// Метод проверяет записана ли строка в префиксной форме
        /// </summary>
        /// <param name="value">Проверямая строка</param>
        /// <returns>True - правильно или False - неправильно</returns>
        private bool Solve(String value)
        {
            // Один ли символ остался в строке
            if (value.Length == 1)
            {
                // Проверяем является ли он параметром 
                return IsParameter(value[0]);
            }
            else if (value.Length > 1)
            {
                // Проверка на количество символов в строке
                int index = LastOperationIndexOf(value);

                // Проверка найден ли символ операции
                if (index > -1)
                {
                    // Унарный ли операции символ
                    if (IsUnaryOperation(value[index]))
                    {
                        // Унарная операция не на последнем месте, заменяем её
                        if (value.Length >= index + 2)
                        {
                            // Является ли параметром символ следующий за символом операции 
                            if (IsParameter(value[index + 1]))
                            {
                                // Если является то заменяем символ операции и следующий за ним символ на Z 
                                // и вызываем проверку обновленной строки
                                value = value.Substring(0, index) + 'Z' + value.Substring(index + 2);
                                return Solve(value);
                            }
                        }
                    }
                    // Бинарной ли операции символ
                    else if (IsBinaryOperation(value[index]))
                    {
                        // Бинарная операция не на последнем или предпоследнем месте, заменяем её
                        if (value.Length >= index + 3)
                        {
                            // Является ли параметрами два символа следующие за символом операции                   
                            if (IsParameter(value[index + 1]) && IsParameter(value[index + 2]))
                            {
                                // Если является то заменяем символ операции и следующие за ним два символ на Z 
                                // и вызываем проверку обновленной строки
                                value = value.Substring(0, index) + 'Z' + value.Substring(index + 3);
                                return Solve(value);
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Метод <c>LastOperationIndexOf</c> возвращает позицию символа последней операции в строке <c>value</c>
        /// </summary>
        /// <param name="value">Строка</param>
        /// <returns>Позицию символа последней операции или -1 - если символа операций в строке нет</returns>
        private int LastOperationIndexOf(String value)
        {
            for (int i = value.Length -1; i >= 0; i--)
            {
                if (IsBinaryOperation(value[i]) || IsUnaryOperation(value[i]))
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Метод <c>IsParameter</c> проверяет является ли символ <c>value</c> параметром
        /// </summary>
        /// <param name="value">Символ</param>
        /// <returns>True - является или False - еявляется</returns>
        private bool IsParameter(char value)
        {
            return value >= 65 && value <= 90;
        }

        /// <summary>
        /// Метод <c>IsParameter</c> проверяет является ли символ <c>value</c> символом бинарной операцией
        /// </summary>
        /// <param name="value">Символ</param>
        /// <returns>True - является или False - еявляется</returns>
        private bool IsBinaryOperation(char value)
        {
            if (value == 42 ||       // умножение *
                value == 43 ||       // сложение +
                value == 45 ||       // разность -
                value == 47)         // деление /
            {
                return true;
            } else
            {
                return false;
            }
        }

        /// <summary>
        /// Метод <c>IsParameter</c> проверяет является ли символ <c>value</c> символом унарной операцией
        /// </summary>
        /// <param name="value">Символ</param>
        /// <returns>True - является или False - еявляется</returns>
        private bool IsUnaryOperation(char value)
        {
            if (value == 35)        // квадратный корень #
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

